//
//  ContentView.swift
//  stay-awake-frontend
//
//  Created by William Trevis on 11/01/2022.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

//
//  stay_awake_frontendApp.swift
//  stay-awake-frontend
//
//  Created by William Trevis on 11/01/2022.
//

import SwiftUI

@main
struct stay_awake_frontendApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
